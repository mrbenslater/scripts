#Prestuff
export LANG=en_US.UTF-8
xmodmap /etc/skel/.Xmodmap
hsetroot -cover /usr/share/pyra/wallpapers/official/pyra-a-720.png

#Change default extlinux options
sed -i s/omapdss.def_disp=lcd/omapdss.def_disp=hdmi/ pyra-extlinux
sed -i s/drm_kms_helper.fbdev_rotation=8// #Disable Tiler alltogether

#Avoid doing the first run wizard crap ever
touch /var/lib/pyra/first-run.done

useradd -m --shell /bin/bash -c "Ben Slater,,," -G sudo,audio,bluetooth,plugdev "ben"
passwd "ben"

#Hostname Settings
hostnamectl set-hostname uevm
# update /etc/hosts
cp /etc/hosts /etc/hosts.back
{
echo "127.0.0.1        localhost uevm"
sed '/^127.0.0.1/d' /etc/hosts.back
}>/etc/hosts


# Audio Settings
/usr/share/pyra/scripts/pyra-init-alsa.sh

#Slim Theme
sed -i "s/.*current_theme.*/current_theme pyra/g" /etc/slim.conf

dpkg-reconfigure openssh-server
dpkg-reconfigure -fgnome keyboard-configuration
dpkg-reconfigure -fgnome tzdata
dpkg-reconfigure -fgnome locales
