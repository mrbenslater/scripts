#!/bin/bash

#variables
MASApps=("424389933" "946399090" "605732865" "409183694" "405399194" "1333542190" "682658836" "1085114709" "494803304" "585829637" "411643860" "1262957439" "407963104" "409203825" "1147396723" "409201541" "1402042596" "1295203466" "803453959" "424390742" "430255202" "497799835")


#Fetch HomeBrew

fetchHomeBrew()
{
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}
#fetch mas, gcc etc
brewInstall()
{
brew install mas gcc
}
#fetch mas apps
masApps()
{

for i in "${MASApps[@]}"

do :
	mas install $i
done

}

#adjust system preferences
updateSysPrefs()
{

	#global

	#Disable Auto Correct
	defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false
	defaults write NSGlobalDomain AppleShowAllExtensions -bool true
#dock
	#Enable Dock Autohide
	defaults write com.apple.Dock autohide -bool true

	#Enable Dock Magnification
	defaults write com.apple.Dock magnification -bool true

#Safari
	#Disable Opening Safe Files immediately
	defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

	#Disable Frequently visited sites
	defaults write com.apple.Safari ShowFrequentlyVisitedSites -bool false

	#Enable full URL in search field
	defaults write ShowFullURLInSmartSearchField -bool true

	#Disable password Autofill
	defaults write com.apple.Safari AutoFillPasswords -bool false

	#Finder
	defaults write com.apple.Finder ShowExternalHardDrivesOnDesktop -bool true
	defaults write com.apple.Finder ShowHardDrivesOnDesktop -bool true
	defaults write com.apple.Finder ShowRemovableMediaOnDesktop -bool true
	defaults write com.apple.Finder ShowMountedServersOnDesktop -bool true
	defaults write com.apple.Finder FXEnableExtensionChangeWarning -bool false
	defaults write com.apple.Finder FXDefaultSearchScope -string "SCcf"


}

#todo
#sign into iCloud
#Fetch VLC
#Fetch Fetch
#Clear Stickies
#configure textual

echo "install Xcode select"
sudo xcode-select --install
echo "Fetching Homebrew"
fetchHomeBrew
echo "Brew install stuff"
brewInstall
echo "Fetching MAS Apps"
masApps
echo "Setting Preferences"
updateSysPrefs
