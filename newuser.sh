#!/bin/bash


echo "User Setup application"
echo "Enter Email Address:"
read "emailaddress"
echo "Enter Job Code:" 
read "jobcode"
echo "Enter Manager's name"
read "manager"


firstName=$( echo $emailaddress | cut -d "." -f1 )
lastName=$( echo $emailaddress | cut -d "." -f2 | cut -d "@" -f1 )
site=$( echo $emailaddress | cut -d "@" -f2 | cut -d "." -f1 )
basedir=$PWD 
# User Details Generate
PASS1=$(cat features | sort -R | head -1)
PASS2=$(cat fruits | sort -R | head -1)
NUM=$( echo $RANDOM | cut -c1-4)

username=$firstName.$lastName
mkdir "$username"


# Generate Notes account
echo "$lastName;$firstName;;;macdow;%USERPROFILE%\OneDrive - McConnell Dowell Constructors (Aust.) Pty Ltd\Documents\Lotus IDs;;;;;;;$emailaddress@exchange.migration;;;$emailaddress;;;;;" > $username/$firstName-notes.txt

#Generate Phone request
cat > "$username/$firstName-phone.txt" << EOF

Hi Star21,

Can you please arrange the following:

Handset: iPhone 11 128GB
Plan: \$100 Plan with new number burnt to Sim Card.

Accessories: Case, Screen Protector attached, 20W Charger

For the following person:
Customer: $firstName $lastName
Job Code: $jobcode
Address: ATTN $firstName $lastName C/O $manager {ADDRESS}

Please notify cc'd parties via email when shipment has been sent.

EOF

# Check for site
case $site in
    builtenvirons)
    emailpfix=builtenvirons.com.au
    ;;
    mcdgroup)
    emailpfix=mcdgroup.com
    ;;
    JVAlliance)
    emailpfix=jvalliance.net
    jvusr=$username@emailpfix
    ;;
esac


cat > "$username/$firstName-creds.txt" << EOF
 ---- Credential Email ----
 Dear $manager, 
 Please find the credentials for $firstName $lastName below:

Name: $firstName $lastName
User Name: $username
Password: $PASS1$PASS2$NUM
Email Address:$username@$emailpfix

---- AD Telephone Notes Field ----
{
  "JobCode": "AU:$jobcode",
  "Example1": "abc",
  "Example2": "def",
  "Example3": "ghi"
}

---- AD Attribute Editor ----

ProxyAddress Options:
SMTP:$firstName.$lastName@$emailpfix
smtp:$firstName.$lastName@mcdgroupau.mail.onmicrosoft.com

TargetAddress Options:
SMTP:$firstName.$lastName@mcdgroupau.mail.onmicrosoft.com

EOF
